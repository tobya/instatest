## InstaSupply Test Project ##
* Test project per requirements, allows registeration of users

#### Heroku Public Url ####
* https://evening-shore-80360.herokuapp.com/

#### How do I get set up? ####
* requires ruby 2.2.3 rvm or other ruby version manger should tell you how to install
* written on kubuntu 15.10
* `git clone ...`
* `cd instatest`
* `rvm-prompt` should show "ruby-2.2.3@instatest"
* `gem install bundle`
* `bundle`
* possibly `db:migrate'
* requires firefox for cucumber test

#### Notes ####
* running with sqlite for easy setup just in case postgres not installed - normally would usesame database in all environements per 12 factor app
* postgres gem installed for heroku, comment out in Gemfile if causing problems locally
* cucumber tests are in features, run with `cucumber`
* javascript tag in feature file runs tests in firefox with javascript and opens firefox
* with out javascript it runs in  headless non-js browser
* failed client validations shown as input tooltip (html title)
* missing - serverside data of birth validation, though only accepts valid date
* issues with very narrow screen width - responsive design on mobile screen

#### Who do I talk to? ####
* Toby Aldridge
* toby.aldridge@gmail.com
class User < ActiveRecord::Base
  has_secure_password
  validates :email, uniqueness: true, presence:true,
    format: { with: /\S+@\S+\.\S+/, message: "not a valid email address" }
  end

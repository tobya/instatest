var user = (
  function(){ 
    return {
      displayValid: function(input,  invalidMessage){
        if (invalidMessage === '') {
          // no message means valid
          input.css('background-color', 'white');
        } else {
          input.css('background-color', 'pink');
          $('#client-errors').append(invalidMessage + '<br/>');
        }
        input.prop('title', invalidMessage);
      },
      validateEmail: function(){
        this.displayValid($('#user_email'), '')
        var emailRegExp = /\S+@\S+\.\S+/;  //simple email validation from stack overflow
        var email = $('#user_email').val();
        invalidMessage = emailRegExp.test(email) ? '' : 'Not a valid email address'
        this.displayValid($('#user_email'), invalidMessage);
        return (invalidMessage === '');
      },
      validatePassword: function(){
        this.displayValid($('#user_password'), '');
        this.displayValid($('#user_password_confirmation'), '');
        var password = $('#user_password').val();
        var password_confirmation = $('#user_password_confirmation').val();
        if (password === '') {
          this.displayValid($('#user_password'), 'Please enter a password');
          return false;
        }
        if (password !== password_confirmation) {
          this.displayValid($('#user_password_confirmation'), 'Entered passwords do not match');
          return false;
        }
        return true;
      },
      validateDob: function(){
        this.displayValid($('#user_date_of_birth'), '');
        var dob = $('#user_date_of_birth').val();
        if (dob === '') {
          this.displayValid($('#user_date_of_birth'), ''); //redundant but left for the moment
          return true;
        }
        var dobRegExp = /^\d{1,2}\/\d{1,2}\/\d{4}$/;  //check right pattern
        if (!dobRegExp.test(dob)) {
          this.displayValid($('#user_date_of_birth'), 'Not a date in format dd/mm/yyyy');
          return false;
        }
        var parts = dob.split("/");
        var day = parseInt(parts[0], 10);
        var month = parseInt(parts[1], 10);
        var year = parseInt(parts[2], 10);
        if (month > 12 || day > 31) { // could include extra to check no. of days by month
          this.displayValid($('#user_date_of_birth'), 'Not a valid date');
          return false; 
        } 
        return true;
      },
      submitClicked: function(){
        $('#client-errors').html('');
        //avoid short-circuiting
        emailOk = this.validateEmail();
        passwordOk = this.validatePassword();
        dobOk = this.validateDob();
        if (emailOk && passwordOk && dobOk) {
           //$('#client-errors').addlass('hidden')
           return true;
        } 
        $('#client-errors').removeClass('hidden')
        return false;
      }      
    };
  }
)(); 

$(document).ready(function() {
  $('#user_register').on('click', function(){
    return user.submitClicked();    
  });
});
  Feature: User Validation
    Check user validation

  @javascript
  Scenario: Client side validation of invalid email address works
    Given I am on the default page
    When I fill in the field "Email" with "a@a"
    And I click the "Register" button
    Then the page does not contain the text "User was successfully registered."

  @javascript
  Scenario: Client side validation of valid email address works
    Given I am on the default page
    When I fill in the field "Email" with "a@a.com"
    And I fill in the field "Password" with "a"
    And I fill in the field "Confirm password" with "a"
    And I click the "Register" button
    Then the page contains the text "User was successfully registered."  

  @javascript
  Scenario: Client side validation of valid password address works
    Given I am on the default page
    When I fill in the field "Email" with "a@a.com"
    And I fill in the field "Password" with ""
    And I fill in the field "Confirm password" with ""
    And I click the "Register" button
    Then the page does not contain the text "User was successfully registered." 
    When I fill in the field "Password" with "a"
    And I fill in the field "Confirm password" with "b"
    And I click the "Register" button 
    Then the page does not contain the text "User was successfully registered." 

  Scenario: Server side validation of unique email address works
    Given I am on the default page
    When I fill in the field "Email" with "a@a.com"
    And I fill in the field "Password" with "a"
    And I fill in the field "Confirm password" with "a"
    And I click the "Register" button
    Then the page contains the text "User was successfully registered."  
    Given I am on the default page
    When I fill in the field "Email" with "a@a.com"
    And I fill in the field "Password" with "a"
    And I fill in the field "Confirm password" with "a"
    And I click the "Register" button
    Then the page contains the text "Email has already been taken"     

  Scenario: Server side validation of blank email address works
    Given I am on the default page
    When I fill in the field "Email" with ""
    And I click the "Register" button
    Then the page does not contain the text "User was successfully registered."
    And the page contains the text "Email can't be blank"

  Scenario: Server side validation of invalid email address works
    Given I am on the default page
    When I fill in the field "Email" with "a@a"
    And I click the "Register" button
    Then the page does not contain the text "User was successfully registered."
    And the page contains the text "Email not a valid email address"
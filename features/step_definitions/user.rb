Given(/^I am on the default page$/) do
  visit '/'
end

When(/^I fill in the field "(.*?)" with "(.*?)"$/) do |field_label, field_text|
  fill_in field_label, with: field_text
end

When(/^I click the "([^"]*)" button$/) do |button_text|
   click_button button_text
end

Then(/^field "([^"]*)" is invalid$/) do |arg1|
  pending # Write code here that turns the phrase above into concrete actions
end

Then(/^the page does not contain the text "([^"]*)"$/) do |absent_text|
   assert !has_content?(absent_text)
end

Then(/^the page contains the text "(.*?)"$/) do |text|
  assert has_content?(text)
end

When(/^I pause$/) do
  byebug
end
